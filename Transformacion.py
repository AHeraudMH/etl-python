import os
from datetime import datetime, timedelta
import pandas as pd
import csv

fecha_actual = datetime.today().date().strftime("%d-%m-%Y")
#2024-04-27

print(fecha_actual)
#fecha_inicial_str = f"{fecha_actual}"
#fecha_inicial_str = f"{i:02d}-03-2024"


# Convertir la fecha inicial a objeto datetime
fecha_formatDate = datetime.strptime(fecha_actual, "%d-%m-%Y")

fechaAYER = (fecha_formatDate - timedelta(days=0)).strftime("%d-%m-%Y")

fecha_formateadaHOY = (fecha_formatDate - timedelta(days=1)).strftime("%d-%m-%Y")
fecha_formateadaConsolidado = (fecha_formatDate - timedelta(days=1)).strftime("%d%m%Y")

fecha_formateadaAYER = (fecha_formatDate - timedelta(days=2)).strftime("%d-%m-%Y")
fechaHOY= (fecha_formatDate + timedelta(days=1)).strftime("%d-%m-%Y")
print(fecha_formateadaAYER)
print(fecha_formateadaHOY)



#patron = re.compile(r'^ONT_2.*$')
#archivos_ONT_2 = []
archivo_ont = []
archivo_intents = []
archivo_zte = []
archivo_Osp = []
archivo_Bss = []


for archivo in os.listdir(f"C:/Users/Desktop/CONSOLIDADOS InOuPUT/{fecha_formateadaHOY}"):
    #print(f"ONT_{datetime.today().year}")
    #if archivo[:8]==f"ONT_{datetime.today().year}":
    
    
    if archivo[:5]==f"ONT_2":
        archivo_ont = archivo

        #csv2 = pd.read_csv(f"C:/Users/Desktop/CONSOLIDADOS InOuPUT/{fecha_formateadaHOY}/{archivo}", delimiter=",", usecols=['Object Name','NE','Operational State','Serial Number','Subscriber Location ID','Administrative State','Last Change Date'])
        #print(csv2)
    #if archivo[:16]==f"ONT-INTENTS-{datetime.today().year}":

    
    if archivo[:13]==f"ONT-INTENTS_2" or archivo[:13]==f"ONT-INTENTS-2":
        archivo_intents = archivo
        #csv4 = pd.read_csv(f"C:/Users/Desktop/CONSOLIDADOS InOuPUT/{fecha_formateadaHOY}/{archivo}", skiprows=1, quoting=csv.QUOTE_NONE)
        #print(csv4)
    
    if archivo[:21]==f"Reporte_Ocupacion_OSP":
        archivo_Osp = archivo


    if archivo[:21]==f"Reporte_Ocupacion_BSS":
        archivo_Bss = archivo
    

    if archivo[:11]=="OnuResource":
        archivo_zte.append(archivo)


csv2 = pd.read_csv(fr"C:/Users/Desktop/CONSOLIDADOS InOuPUT/{fecha_formateadaHOY}/{archivo_ont}", delimiter=",", usecols=['Object Name','NE','Operational State','Serial Number','Subscriber Location ID','Administrative State','Last Change Date'])
csv4 = pd.read_csv(f"C:/Users/Desktop/CONSOLIDADOS InOuPUT/{fecha_formateadaHOY}/{archivo_intents}", usecols=['_source.target.ont-name','_source.configuration.expected-registration-id','_source.configuration.fiber-name','_source.sync-timestamp','_source.configuration.onu-id','relative-object-id'], dtype={"_source.configuration.onu-id":'Int8','_source.configuration.expected-registration-id':'string'})
csv5HistoricLineIdVnoCode = pd.read_csv(rf"c:/Users/Desktop/FACT_NOV/HistoricoLineIdVnoCodeAll/HistoricoLineIdVnoCodeAll 18052024.csv", usecols=['ID', 'ID2'], dtype={"ID":"string", "ID2":"string"})
csv4BAJAS_ALL = pd.read_csv("C:/Users/Desktop/Heraud_Works/BajasAcumuladas/Bajas_Acumuladas_Consolidado.csv", delimiter=";", usecols=['FECHA_PEDIDO_BAJA', 'ID'], dtype={"ID":"string"})
csvOsp = pd.read_csv(f"C:/Users/Desktop/CONSOLIDADOS InOuPUT/{fecha_formateadaHOY}/{archivo_Osp}", sep=",", usecols=['ID','ID2','NODO','DIVICAU','CTO'], dtype={"ID":"string","ID2":"string","BORNE":"string","DIVICAU":"string"})
csvBss = pd.read_csv(f"C:/Users/Desktop/CONSOLIDADOS InOuPUT/{fecha_formateadaHOY}/{archivo_Bss}", sep=",", usecols=['ID','ID2','BROADBAND','VOIP','IPTV'], dtype={"ID":"string","ID2":"string","BROADBAND":"string","VOIP":"string","IPTV":"string"})
csvIsp = pd.read_csv(f"C:/Users/Desktop/Heraud_Works/zzFotoPangeaTdp.csv", sep=",", dtype={"OLT_ID":"string"})
csv5BlackList = pd.read_excel("C:/Users/Desktop/Heraud_Works/Clientes Reconfigurados.xlsx", usecols=['ID'])
csvFotoIsp = pd.read_csv(f"C:/Users/Desktop/Heraud_Works/Inventario_Foto_Pangea/InventarioPangea_Foto_tdp_20240524_040000.csv", sep="~", usecols=['ID','SERVICE_TYPE','BRASDEVICE','NASPORT','PORT_ID'], dtype={'PORT_ID':'string'})

csvFotoIspNotNan = csvFotoIsp[csvFotoIsp['NASPORT'].notna()]
csvFotoIspNotNanDropDup = csvFotoIspNotNan.drop_duplicates(subset=['ID','SERVICE_TYPE','NASPORT'])

#print(csvFotoIspNotNan)


csvOspDropDup = csvOsp.drop_duplicates(subset=["ID","ID2"])

#AQUÍ
#unionOspBss = pd.merge(csvOspDropDup, csvBss, how='inner', on=['ID','ID2'])
unionOspBss = pd.merge(csvBss, csvOspDropDup, how='left', on=['ID','ID2'])

csv4BAJAS_ALL['FECHA_PEDIDO_BAJA'] = pd.to_datetime(csv4BAJAS_ALL['FECHA_PEDIDO_BAJA']).dt.tz_convert('America/Lima')
ordenDateBajaALL = csv4BAJAS_ALL.sort_values(by='FECHA_PEDIDO_BAJA')

BajasOrder = ordenDateBajaALL[ordenDateBajaALL['FECHA_PEDIDO_BAJA']<'2024-05-20']
BajasOrderLast = BajasOrder.drop_duplicates(subset=["ID"], keep='last')

BajasOrderLastFiltBlack = BajasOrderLast[~BajasOrderLast["ID"].isin(csv5BlackList["ID"])]


#csv5HistoricLineIdVnoCodeONU = csv5HistoricLineIdVnoCode.rename(columns={'ID': 'ONU_NAME'})

encabezados = ['ID','TOKEN','OLT_COMPLET','ONU_ID','FECHA_INSTALACION','ONU_ID_IACM']

csv4.columns = encabezados

#csv4['ONU_ID'] = csv4['ONU_ID'].str[:-2]

csv4[['TARJETA', 'PUERTO']] = csv4['OLT_COMPLET'].str.extract(r'-(\d+)-(\d+)$')

#csv4[['dato1', 'dato2', 'dato3', 'ONU_ID_ALT']] = csv4['ONU_ID_IACM'].str[8:].str.split('.', expand=True)
csv4['ONU_ID_IACM2'] = csv4['ONU_ID_IACM'].str[16:]
csv4[['dato1', 'dato2', 'dato3', 'ONU_ID2']] = csv4['ONU_ID_IACM2'].str.split('.', expand=True)

csv4['ONU_ID2'] = csv4['ONU_ID2'].str[3:]

#csv4['ONU_ID'] = csv4['ONU_ID'].str.replace(' ', '--') 

#csv4['ONU_ID'] = csv4.apply(lambda row: row['ONU_ID2'] if row['ONU_ID'] == " " else row['ONU_ID'], axis=1)
csv4['ONU_ID'].fillna(csv4['ONU_ID2'], inplace=True)

csv4['llave'] = csv4['OLT_COMPLET']+'-'+csv4['ONU_ID'].astype(str)


csv2['llave'] = csv2['Object Name'].str.replace(':R', '-').str.replace('.S', '-').str.replace('.LT', '-').str.replace('.PON', '-').str.replace('.ONT', '-')


union = pd.merge(csv4, csv2, how='inner', on=["llave"])

union['FECHA_INSTALACION'] = pd.to_datetime(union['FECHA_INSTALACION']).dt.strftime('%Y-%m-%d %H:%M:%S')

union2 = union[['ID','NE','TARJETA','PUERTO','ONU_ID','TOKEN','Serial Number','FECHA_INSTALACION','Last Change Date']]

encabezados2 = ['ID','OLT','TARJETA','PUERTO','ONU_ID','TOKEN','SERIAL_NUMBER','FECHA_INSTALACION', 'FECHA_ULTIMO_CAMBIO']

union2.columns = encabezados2



datos_totales=[]
for archivosZte in archivo_zte:
    datos_csv = pd.read_csv(f"C:/Users/Desktop/CONSOLIDADOS InOuPUT/{fecha_formateadaHOY}/{archivosZte}", sep=",", usecols=['Name', 'Slot','Port', 'ONU ID', 'NE Name', 'Authentication Value', 'MAC/SN', 'Installation Date','Last Offline Time'])
    datos_totales.append(datos_csv) 
    
datos_completos = pd.concat(datos_totales, ignore_index=True)
#print(datos_completos)
#datos_completos['Installation Date'] = pd.to_datetime(datos_completos['Installation Date']).dt.strftime('%Y-%m-%d')
datos_completos2 = datos_completos[['Name','NE Name','Slot','Port','ONU ID','Authentication Value','MAC/SN','Installation Date','Last Offline Time']]
encabezados2_ZTE= ['ID','OLT','TARJETA','PUERTO','ONU_ID','TOKEN','SERIAL_NUMBER','FECHA_INSTALACION', 'FECHA_ULTIMO_CAMBIO']

datos_completos2.columns = encabezados2_ZTE


unionZte_NOkia=pd.concat([datos_completos2,union2], axis=0)

unionZte_NOkia['TOKEN'] = unionZte_NOkia['TOKEN'].str[:14]


#unionZte_NOkia['SERIAL_NUMBER'] = unionZte_NOkia['SERIAL_NUMBER'].str.replace('ALCL00000000', '').str.replace('ABCL00000000', '').str.replace('ALCL00000000', '').str.replace('ALCL00000001', '').str.replace('--', '')


FiltLineId = unionZte_NOkia[unionZte_NOkia['ID'].str.startswith(("PER.TDP"), na=False)]


unionVnoCode = pd.merge(FiltLineId, csv5HistoricLineIdVnoCode, how='left', on=["ID"])


ReconfigToBajasALL = unionVnoCode[~unionVnoCode["ID"].isin(BajasOrderLastFiltBlack["ID"])]

#AQUÍ
LeftDataEnriquecido = pd.merge(ReconfigToBajasALL, unionOspBss, how='left', on=["ID"])




#unionVnoCode2= LeftDataEnriquecido[['ID','ID2','OLT','TARJETA','PUERTO','ONU_ID','TOKEN','SERIAL_NUMBER']]

def condicion_crit2(x):
        if x == "P_OLT_C600":
            return "ZTE"
        elif x == "P_OLT_7360":
            return "NOKIA"
        else:
            return ""

#LeftDataEnriquecido['VNO_CODE_x'].fillna(LeftDataEnriquecido['ONU_ID2'], inplace=True)

LeftDataEnriquecido['GESTOR'] = LeftDataEnriquecido['OLT'].str[:10].apply(condicion_crit2)

#y buena
LeftDataEnriquecido['VNO_CODE_y'].fillna(LeftDataEnriquecido['VNO_CODE_x'], inplace=True)

LeftDataEnriquecidoToIsp = pd.merge(LeftDataEnriquecido, csvIsp, how='left', on=["OLT"])


LeftDataEnriquecidoToIsp['PORT_ID'] = '1-'+'1-'+LeftDataEnriquecidoToIsp['TARJETA'].astype(str)+'-'+LeftDataEnriquecidoToIsp['PUERTO'].astype(str)+'-'+LeftDataEnriquecidoToIsp['ONU_ID'].astype(str)+'-'+LeftDataEnriquecidoToIsp['OLT_ID'].astype(str)


df = LeftDataEnriquecidoToIsp.drop('VNO_CODE_x', axis = 1)

    #df.to_csv(f"./ReporteTDP/ONTS_CONSOLIDADO_{fecha_formateadaConsolidado} PREVIO3.csv", index=False)

dfFinal = df.rename(columns={'VNO_CODE_y':'ID2'})
#union2 = union[['ID','OLT','TARJETA','PUERTO','ONU_ID','TOKEN','SERIAL_NUMBER','FECHA_INSTALACION','FECHA_ULTIMO_CAMBIO','VNO_CODE_y','NODO','DIVICAU','CTO','VNO','BROADBAND','VOIP','IPTV','GESTOR','OLT_ID','PORT_ID']]

#encabezados2 = ['ID','OLT','TARJETA','PUERTO','ONU_ID','TOKEN','SERIAL_NUMBER','FECHA_INSTALACION', 'FECHA_ULTIMO_CAMBIO']

#ID,OLT,TARJETA,PUERTO,ONU_ID,TOKEN,SERIAL_NUMBER,FECHA_INSTALACION,FECHA_ULTIMO_CAMBIO,VNO_CODE_x,VNO_CODE_y,NODO,DIVICAU,CTO,VNO,BROADBAND,VOIP,IPTV,GESTOR,OLT_ID,PORT_ID
dfFinal['BROADBAND'] = dfFinal['BROADBAND'].str.replace('1', 'BB')
dfFinal['VOIP'] = dfFinal['VOIP'].str.replace('1', 'VoIP')
dfFinal['IPTV'] = dfFinal['IPTV'].str.replace('1', 'IPTV')

    #dfFinal.to_csv(f"./ReporteTDP/ONTS_CONSOLIDADO_{fecha_formateadaConsolidado} PREVIO2.csv", index=False)

dfBbVoIpIptv0 = dfFinal[dfFinal['BROADBAND'].isin(['0']) & dfFinal['VOIP'].isin(['0']) & dfFinal['IPTV'].isin(['0'])]

dfBb1 = dfFinal[dfFinal['BROADBAND'].isin(['BB'])]
dfBb2 = dfBb1.assign(SERVICE_TYPE='BB')

dfVoIp1 = dfFinal[dfFinal['VOIP'].isin(['VoIP'])]
dfVoIp2 = dfVoIp1.assign(SERVICE_TYPE='VoIP')

dfIptv1 = dfFinal[dfFinal['IPTV'].isin(['IPTV'])]
dfIptv2 = dfIptv1.assign(SERVICE_TYPE='IPTV')

dfNan = dfFinal[~dfFinal['BROADBAND'].notna()]


joinDFsFinal=pd.concat([dfBbVoIpIptv0, dfBb2, dfVoIp2, dfIptv2, dfNan], axis=0)

    #joinDFsFinal.to_csv(f"./ReporteTDP/ONTS_CONSOLIDADO_{fecha_formateadaConsolidado} PREVIO.csv", index=False)

joinDFsFinalTdp = pd.merge(joinDFsFinal, csvFotoIspNotNanDropDup, how='left', on=['ID','SERVICE_TYPE','PORT_ID'])

joinDFsFinalTdpCheck = joinDFsFinalTdp.drop(['BROADBAND','VOIP','IPTV','FECHA_ULTIMO_CAMBIO'], axis = 1)

#NOKIA  1-1-2-4-8-K006
#ZTE    1-1-3-4-10-T034
#dfFinal.to_csv(f"./ReporteTDP/ONTS_CONSOLIDADO_{fecha_formateadaConsolidado} FiltPER2.csv", index=False)
joinDFsFinalTdpCheck.to_csv(f"./ReporteTDP/ONTS_CONSOLIDADO_{fecha_formateadaConsolidado} FiltPER2 v2.csv", index=False)

