import shutil
import datetime
from zipfile import ZipFile
import os
import paramiko

fecha_actual = datetime.date.today()

fecha_anterior = fecha_actual - datetime.timedelta(days=1)

fecha_formateadaHOY = fecha_actual.strftime('%d-%m-%Y')
fecha_formateada = fecha_anterior.strftime('%d-%m-%Y')
fecha_formateada_sftp = fecha_actual.strftime('%Y%m%d')

dicc_meses = [
    "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio",
    "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"
]


mes_string = dicc_meses[fecha_anterior.month - 1]
mes_numero = fecha_anterior.month
anio_numero = fecha_anterior.year

mes_stringNOKIA = dicc_meses[fecha_actual.month - 1]
mes_numeroNOKIA = fecha_actual.month
anio_numeroNOKIA = fecha_actual.year
#mes_stringUp = mes_string


origenZTE=rf"C:\Users\{mes_numero}. {mes_string.upper()}\{fecha_formateada}"
origenNOKIA=rf"C:\Users\{mes_numero}. {mes_string}\{fecha_formateada}"


origenLine=r"C:\UsersAct"
destino=rf"C:\Users\{origenZTE[-10:]}"


shutil.copytree(origenLine, destino, ignore=shutil.ignore_patterns('*.xlsx'), dirs_exist_ok=True)
shutil.copytree(origenNOKIA, destino, ignore=shutil.ignore_patterns('*.xlsx','*.csv'), dirs_exist_ok=True)
shutil.copytree(origenZTE, destino, ignore=shutil.ignore_patterns('*.xlsx'), dirs_exist_ok=True)

archivo = os.listdir(rf'C:\Users\{origenZTE[-10:]}')

for abc in archivo:
    contador=""
    if abc[-3:]=="zip":
        contador += abc
        print(contador)
        with ZipFile(rf'C:\Users\Desktop\CONSOLIDADOS InOuPUT\{origenZTE[-10:]}\{contador}', "r") as zip:
            zip.extractall(rf'C:\Users\Desktop\CONSOLIDADOS InOuPUT\{origenZTE[-10:]}') 
            zip.close()
            os.remove(rf'C:\Users\Desktop\CONSOLIDADOS InOuPUT\{origenZTE[-10:]}\{contador}')

hostname = 'blob.core.windows.net'
port = 22
username = 'blobStorague'
password = 'xxxxxxxxxxxxxxxxxxxxx'

transport = paramiko.Transport((hostname, port))
transport.connect(username=username, password=password)
transport.default_max_packet_size = 100000000
transport.default_window_size = 100000000
sftp = transport.open_sftp_client()

sistemas = ["osp"]     
for sistema in sistemas:
    remote_file_path = rf'/osp/REPORTE/Reporte_{fecha_formateada_sftp}.csv'
    local_file_path = rf'C:\Users\{origenZTE[-10:]}\Reporte_Ocupacion_OSP_{fecha_formateada_sftp}.csv'
    sftp.get(remote_file_path, local_file_path)

