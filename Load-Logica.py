import pandas as pd
import numpy as np
from datetime import datetime, timedelta



#for i in range(30,32):
#fecha hoy
fecha_inicial_str = f"24-05-2024"
#fecha_inicial_str = f"{i:02d}-03-2024"


# Convertir la fecha inicial a objeto datetime
fecha_formatDate = datetime.strptime(fecha_inicial_str, "%d-%m-%Y")

fechaAYER = fecha_formatDate - timedelta(days=0)
fecha_formateadaHOY = fecha_formatDate - timedelta(days=1)
fecha_formateadaAYER = fecha_formatDate - timedelta(days=2)
fechaHOY= fecha_formatDate + timedelta(days=1)
print(fecha_formateadaAYER)
print(fecha_formateadaHOY)

fechaAYERFormat = fecha_formateadaAYER.strftime("%d%m%Y")
fechaHOYFormat = fecha_formateadaHOY.strftime("%d%m%Y")
fecha_anterior = fecha_formateadaHOY.strftime("%Y-%m-%d")
fecha_formateada = fecha_formateadaHOY.strftime("%d-%m-%Y")


#csv1 = pd.read_csv('ONTS_CONSOLIDADO_07122023.csv', delimiter=',', usecols=[0,6,7])
csv1 = pd.read_csv(fr'./CONSOLIDADO_OLT/ONTS_CONSOLIDADO_{fechaAYERFormat}.csv', delimiter=',', usecols=['ONU_NAME', 'OLT', 'SERIAL_NUMBER', 'FECHA_INSTALACION'], dtype={"ONU_NAME":"string","VNO_CODE":"string"})
#csv1 = pd.read_csv(fr'./CONSOLIDADO_OLT/ONTS_CONSOLIDADO_13032024.csv', delimiter=',', usecols=['ONU_NAME', 'VNO_CODE', 'OLT', 'SERIAL_NUMBER', 'FECHA_INSTALACION'], dtype={"VNO_CODE":"string"})
#csv2 = pd.read_csv('ONTS_CONSOLIDADO_08122023.csv', delimiter=',', usecols=[0,6,7])
csv2 = pd.read_csv(fr'./CONSOLIDADO_OLT/ONTS_CONSOLIDADO_{fechaHOYFormat}.csv', delimiter=',', usecols=['ONU_NAME', 'OLT', 'SERIAL_NUMBER', 'FECHA_INSTALACION'], dtype={"ONU_NAME":"string","VNO_CODE":"string"})
#csv2 = pd.read_csv(fr'./CONSOLIDADO_OLT/ONTS_CONSOLIDADO_15032024.csv', delimiter=',', usecols=['ONU_NAME', 'VNO_CODE', 'OLT', 'SERIAL_NUMBER', 'FECHA_INSTALACION'], dtype={"VNO_CODE":"string"})


csv1['FECHA_INSTALACION'] = csv1['FECHA_INSTALACION'].str.replace('/', '-')
csv1['FECHA_INSTALACION'] = csv1['FECHA_INSTALACION'].str[:-8]
#csv1['DIAS'] = pd.to_datetime(csv1['FECHA_INSTALACION'], format='%Y-%m-%d', exact=False)
#csv1['FECHA_INSTALACION'] =  pd.to_datetime(csv1['FECHA_INSTALACION'], format='%Y-%m-%d', exact=False)
#csv2['FECHA_INSTALACION'] =  pd.to_datetime(csv2['FECHA_INSTALACION'], format='%Y-%m-%d', exact=False)
#csv2['FECHA_INSTALACION'] = csv2['FECHA_INSTALACION'].str.replace('/', '-')
csv2['FECHA_INSTALACION'] = csv2['FECHA_INSTALACION'].str.replace('/', '-')
csv2['FECHA_INSTALACION'] = csv2['FECHA_INSTALACION'].str[:-8]

#csv1['DIAS'] = pd.to_datetime(csv1['FECHA_INSTALACION'], format='%Y-%m-%d', exact=False)
#csv2['FECHA_INSTALACION'] =  pd.to_datetime(csv2['FECHA_INSTALACION'], format='%Y-%m-%d', exact=False)


csv1['DISTRITO'] = csv1['OLT'].str[11:-2]
csv2['DISTRITO'] = csv2['OLT'].str[11:-2]

csv1['VNO_CODE'] = ''
csv2['VNO_CODE'] = ''


def condicion_crit2(x):
    if x == "P_OLT_C600":
        return "ZTE"
    elif x == "P_OLT_7360":
        return "NOKIA"
    else:
        return ""
    
csv1['GESTOR'] = csv1['OLT'].str[:10].apply(condicion_crit2)
csv2['GESTOR'] = csv2['OLT'].str[:10].apply(condicion_crit2)

csv1['OLT'] = csv1['OLT'].str[11:]
csv2['OLT'] = csv2['OLT'].str[11:]

onu_name_diff1 = csv1[~csv1['ONU_NAME'].isin(csv2['ONU_NAME'])]
probando1 = onu_name_diff1[onu_name_diff1['SERIAL_NUMBER'].notna()]
deBaja = probando1.assign(ESTADO='Bajas', FECHA=f'{fecha_anterior}')
#Usuarios que dieron de baja su servicio.
########################

onu_name_diff2 = csv1[~csv1['ONU_NAME'].isin(csv2['ONU_NAME'])]
probando2 = onu_name_diff2[onu_name_diff2['SERIAL_NUMBER'].isin([np.nan])]
cancelados = probando2.assign(ESTADO='Cancelados', FECHA=f'{fecha_anterior}')
#Usuarios que cancelaron su pedido de servicio.
########################

onu_name_diff = csv2[csv2['ONU_NAME'].isin(csv1['ONU_NAME'])]
probando2 = onu_name_diff[onu_name_diff['SERIAL_NUMBER'].isin([np.nan])]
enEspera = probando2.assign(ESTADO='En espera', FECHA=f'{fecha_anterior}')
#Usuarios que siguen en espera de servicio
########################

csv1NanSerial = csv1[csv1['SERIAL_NUMBER'].isin([np.nan])]
cvs2NotnaSerial = csv2[csv2['SERIAL_NUMBER'].notna()]

onu_name_join4 = cvs2NotnaSerial[cvs2NotnaSerial['ONU_NAME'].isin(csv1NanSerial['ONU_NAME'])]
conServicio = onu_name_join4.assign(ESTADO='Instalados', FECHA=f'{fecha_anterior}')
#Usuarios que se les instalo el servicio 
########################

#cvs2NotnaSerial = csv2[csv2['SERIAL_NUMBER'].notna()]

onu_name_join5 = cvs2NotnaSerial[~cvs2NotnaSerial['ONU_NAME'].isin(csv1['ONU_NAME'])]

conServicio2 = onu_name_join5.assign(ESTADO='Instalados', FECHA=f'{fecha_anterior}')
#Usuarios que se les instalo el servicio el mismo día
########################

cvs2NotnanSerial = csv2[csv2['SERIAL_NUMBER'].isin([np.nan])]

onu_name_join6 = cvs2NotnanSerial[~cvs2NotnanSerial['ONU_NAME'].isin(csv1['ONU_NAME'])]

conReserva = onu_name_join6.assign(ESTADO='Reservado', FECHA=f'{fecha_anterior}')
#Usuarios que se les ha realizado una reserva
########################

    #csv1NotnaSerial = csv1[csv1['SERIAL_NUMBER'].notna()]
#cvs2NotnaSerial = csv2[csv2['SERIAL_NUMBER'].notna()]

    #onu_name_join7 = cvs2NotnaSerial[cvs2NotnaSerial['ONU_NAME'].isin(csv1NotnaSerial['ONU_NAME'])]
    #conServicio3 = onu_name_join7.assign(ESTADO='Activo', FECHA=f'{fecha_anterior}')
#Usuarios que se les instalo el servicio 
########################

joinDFs=pd.concat([deBaja, cancelados, enEspera, conServicio, conServicio2, conReserva], axis=0)


joinDFs['DIAS'] = (pd.to_datetime(joinDFs['FECHA']) - pd.to_datetime(joinDFs['FECHA_INSTALACION'], exact=True)).dt.days



def condicion_crit(x):
    if x >= 51:
        return "Mas de 51"
    elif x >= 41:
        return "41-50"
    elif x >= 31:
        return "31-40"
    elif x >= 21:
        return "21-30"
    elif x >= 16:
        return "16-20"
    elif x >= 11:
        return "11-15"
    elif x >= 6:
        return "6-10"
    else:
        return "0-5"
    
joinDFs['CRITICIDAD'] = joinDFs['DIAS'].apply(condicion_crit)

def condicion_chrun(x):
    if x >= 365:
        return "Maduro"
    elif x >= 213:
        return "Premaduro"
    else:
        return "Prematuro"
    
joinDFs['CHURN'] = joinDFs['DIAS'].apply(condicion_chrun)


def condicion_vno(x):
    if 'TDP' in x:
        return 'TDP'
    elif 'VNO01' in x:
        return 'Iway'
    elif 'VNO02' in x:
        return 'Waoo'
    else:
        return x
    
joinDFs['VNO'] = joinDFs['ONU_NAME'].apply(condicion_vno)

joinDFs.to_excel(rf'./ConsolidadosDataComercial/ConsolidadoEstado {fecha_formateada}.xlsx', sheet_name='Sheet1', index=False)
#joinDFs.to_excel(rf'NuevoPROCESO {fecha_formateada}.xlsx', sheet_name='Sheet1', index=False)